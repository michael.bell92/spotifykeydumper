#include "pch.h"
#include "Utils.h"
#include "Hooks.h"
#include <fstream>

typedef int (__cdecl* keyToLE_v25)(unsigned int* dest, int* key, int bits);
keyToLE_v25 keyToLE_v25_hook = nullptr;

typedef int (__cdecl* keyToLE_v28)(unsigned int* dest, int* key, int bits, bool isEncoded);
keyToLE_v28 keyToLE_v28_hook = nullptr;

typedef int (__thiscall* authTokenHook_v45)(void* This, char* authToken, int a3, int a4, int a5, size_t tokenLength,
	int a7, int a8, int a9, __int64 bearerStr, int a11, int a12, int a13, int a14, char a15, int a16, int int_40,
	int a18, char a19, int a20);
authTokenHook_v45 authToken_v45_hook = nullptr;

typedef char* (__cdecl* getFileId_v45)(char* dest, char* src, size_t srcLength);
getFileId_v45 getFileId_v45_hook = nullptr;

std::string prevKeyStr = std::string();
std::string keyslocation = "keys.txt";

int __cdecl keyToLE_hook_v25(unsigned int* dest, int* key, int bits)
{
	if (bits == 128)
	{
		BYTE keyBuffer[16];
		BYTE* keyBufPtr = keyBuffer;
		memcpy(keyBufPtr, key, 16);

		// Only print out key if it is different
		std::string newKeyStr = Utils::HexString(keyBufPtr, 16);
		if (newKeyStr.compare(prevKeyStr) != 0)
			std::cout << "Key: " << newKeyStr << std::endl << std::endl;


		prevKeyStr = newKeyStr;
	}

	return keyToLE_v25_hook(dest, key, bits);
}

int __cdecl keyToLE_hook_v28(unsigned int* dest, int* key, int bits, bool isEncoded)
{
	if (bits == 128)
	{
		void* decodedKeyPtr = key;

		if (isEncoded)
		{
			// key is encoded with some sort of algorithm; decode it here

			unsigned int keyDecoded[4];
			unsigned int uVar1;
			unsigned int keyPtr;
			unsigned int uVar3;
			int index;

			keyPtr = *key;
			index = 15;
			uVar3 = key[1];
			keyDecoded[0] = key[2];
			keyDecoded[1] = key[3];
			keyDecoded[2] = key[4];
			keyDecoded[3] = key[5];

			do
			{
				uVar1 = keyDecoded[index - 1 & 3];
				keyDecoded[index & 3] =
					keyDecoded[index & 3] +
					(((((uVar1 + index + keyPtr & uVar1 * 16 + uVar3) * 2 + uVar1 * -17) -
						index) - uVar3) - keyPtr);

				index--;
			}
			while (index >= 0);

			decodedKeyPtr = &keyDecoded;
		}

		// Copy key bytes to new buffer
		unsigned char keyBuffer[16];
		unsigned char* keyBufPtr = keyBuffer;
		memcpy(keyBufPtr, decodedKeyPtr, 16);

		// Only print out key if it is different
		std::string newKeyStr = Utils::HexString(keyBufPtr, 16);
		if (newKeyStr.compare(prevKeyStr) != 0) {
			std::cout << "Key: " << newKeyStr << std::endl << std::endl;
			std::ofstream keysfile;
			keysfile.open(keyslocation, std::ios_base::app);
			std::string final_output = newKeyStr + "\n";
			keysfile.write(final_output.c_str(), strlen(final_output.c_str()));
			keysfile.close();

		}
		prevKeyStr = newKeyStr;
	}

	return keyToLE_v28_hook(dest, key, bits, isEncoded);
}

int __fastcall authTokenHook_hook_v45(void* This, void* _EDX, char* authToken, int a3, int a4, int a5,
	size_t tokenLength, int a7, int a8, int a9, __int64 bearerStr, int a11, int a12, int a13, int a14, char a15,
	int a16, int int_40, int a18, char a19, int a20)
{
	if (tokenLength == 311)
	{
		std::cout << "Auth token: " << authToken << std::endl << std::endl;
	}

	return authToken_v45_hook(This, authToken, a3, a4, a5, tokenLength, a7, a8, a9, bearerStr, a11, a12, a13, a14, 15,
		a16, int_40, a18, a19, a20);
}

char* __cdecl hexToStr_hook_v45(char* dest, char* src, size_t srcLength)
{
	if (srcLength == 20)
	{
		// Assume it is file id
	}

	return getFileId_v45_hook(dest, src, srcLength);
}

char* GetKeyFuncAddrV26()
{
	BYTE ref_v19 = 0x55;
	BYTE* byteAtAddrStr = (BYTE*)0x010800C0;

	// Byte at byteAtAddr in 1.1.26-19 is 0x55
	if (*byteAtAddrStr == ref_v19)
		return (char*)0x010800C0;
	else
		return (char*)0x0107FEC0;
}

char* GetKeyFuncAddrV27()
{
	BYTE ref_v7 = 0x55;
	BYTE* byteAtAddrStr = (BYTE*)0x01068F90;

	// Byte at byteAtAddr in 1.1.27-7 is 0x55
	if (*byteAtAddrStr == ref_v7)
		return (char*)0x01068F90;
	else
		return (char*)0x01068F20;
}

void Hooks::Init()
{
	int spotifyVer = Utils::GetSpotifyVersion();

	// Method is stripped from Release build if this isn't here :/
	std::cout << "Spotify version: " << Utils::GetSpotifyVersion() << std::endl;

	switch (spotifyVer)
	{
	case 25:
		keyToLE_v25_hook = (keyToLE_v25)Utils::TrampHook32((char*)0x0106B920, (char*)keyToLE_hook_v25, 6);
		break;
	case 26:
		// Two 1.1.26 versions
		keyToLE_v25_hook = (keyToLE_v25)Utils::TrampHook32(GetKeyFuncAddrV26(), (char*)keyToLE_hook_v25, 6);
		break;
	case 27:
		// Two 1.1.27 versions
		keyToLE_v25_hook = (keyToLE_v25)Utils::TrampHook32(GetKeyFuncAddrV27(), (char*)keyToLE_hook_v25, 6);
		break;
	case 28:
		keyToLE_v28_hook = (keyToLE_v28)Utils::TrampHook32((char*)0x01074650, (char*)keyToLE_hook_v28, 6);
		break;
	case 29:
		keyToLE_v28_hook = (keyToLE_v28)Utils::TrampHook32((char*)0x010861B0, (char*)keyToLE_hook_v28, 6);
		break;
	case 30:
		keyToLE_v28_hook = (keyToLE_v28)Utils::TrampHook32((char*)0x0108E840, (char*)keyToLE_hook_v28, 6);
		break;
	case 44:
		keyToLE_v28_hook = (keyToLE_v28)Utils::TrampHook32((char*)0x010CABC0, (char*)keyToLE_hook_v28, 6);
		break;
	case 45:
		keyToLE_v28_hook = (keyToLE_v28)Utils::TrampHook32((char*)0x010CF780, (char*)keyToLE_hook_v28, 6);
		//authToken_v45_hook = (authTokenHook_v45)Utils::TrampHook32((char*)0x00EBA4F0, (char*)authTokenHook_hook_v45, 5);
		//authToken_v45_hook = (authTokenHook_v45)Utils::TrampHook32((char*)0x00EBA4F0, (char*)authTokenHook_hook_v45, 5);
		break;
	case 46:
		keyToLE_v28_hook = (keyToLE_v28)Utils::TrampHook32((char*)0x010C2FB0, (char*)keyToLE_hook_v28, 6);
		break;
	}
}