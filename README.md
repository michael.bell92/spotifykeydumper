# SpotifyKeyDumper
### By [@ProfessorTox](https://twitter.com/ProfessorTox)
Dump AES keys for Spotify songs from a compatible Windows Spotify version (compatibility listed below).

![Screenshot Example](./screenshot_example.png)

## Using
1. Make sure `SpotifyKeyDumperInjector.exe` and `SpotifyKeyDumper.dll` are located in the same folder as Spotify (`Spotify.exe`).
2. Start SpotifyKeyDumperInjector either before or after launching Spotify.

## Building
This project uses C++14 on Visual Studio 2019

## Compatibility
* 1.1.25
* 1.1.26
* 1.1.27
* 1.1.28
* 1.1.29
* 1.1.30
* 1.1.44
* 1.1.45
* 1.1.46

If you want a specific version, DM me.

## Notes
* Tools used for research: IDA Pro, Ghidra, and Cheat Engine
* **This program was created for educational purposes. It is not intended to be used otherwise**

## License
The MIT License (MIT)